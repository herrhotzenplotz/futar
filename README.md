# futar

Silly tar implementation aus Jux und Tollerei.

## Building

Just run make.

## License

2-Clause BSD license (aka. FreeBSD license).

## Notes

The code currently depends on BSD functionality. I don't know if it
works on Linux, Haiku, Solaris, Illumos or any of your favorite other
OSes.
