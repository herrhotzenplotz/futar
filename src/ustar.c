/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ustar.h"
#include "pw.h"
#include "tio.h"

#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static size_t
parse_octal(const char *buffer, size_t buffer_size)
{
	char *endptr  = NULL;
	char  tmp[32] = {0};

	/* We strictly require the buffer to be smaller than tmp as we
	 * NEED the null terminator. */
	assert(buffer_size < sizeof(tmp));

	memcpy(tmp, buffer, buffer_size);

	return strtoul(tmp, &endptr, 8);
}

int
ustar_read_header(const struct header_posix_ustar *header,
				  tcb *out)
{
	if (!out)
		return -1;

	/* Make sure that we are now looking at a ustar archive */
	if (!is_ustar(header))
		return -1;

	out->name  = strdup(header->name);
	out->mode  = (mode_t)(parse_octal(header->mode, sizeof(header->mode)));
	out->uid   = (uid_t)(parse_octal(header->uid, sizeof(header->uid)));
	out->gid   = (gid_t)(parse_octal(header->gid, sizeof(header->gid)));
	out->size  = parse_octal(header->size, sizeof(header->size));
	out->mtime = (time_t)parse_octal(header->mtime, sizeof(header->mtime));

	switch (*header->typeflag) {
	case '1': out->type = TCB_TYPE_HARDLINK; break;
	case '2': out->type = TCB_TYPE_SYMLINK;  break;
	case '3': out->type = TCB_TYPE_CDEV;     break;
	case '4': out->type = TCB_TYPE_BLOCKDEV; break;
	case '5': out->type = TCB_TYPE_DIR;      break;
	case '6': out->type = TCB_TYPE_NPIPE;    break;
	case '7': out->type = TCB_TYPE_RESERVED; break;
	default:
		warnx("warning: unrecognized typeflag '%c'", *header->typeflag);
		/* FALLTHRU */
	case '0': out->type = TCB_TYPE_REGULAR;  break;
	}

	/* Try to resolve the user id */
	if (*header->uname) {
		/* Damn NUL-terminators are a pain in the ass */
		char unamebuf[sizeof(header->uname) + 1] = {0};
		memcpy(unamebuf, header->uname, sizeof(header->uname));

		pw_getuid(unamebuf, &out->uid);
	}

	/* Try to resolve the group id */
	if (*header->gname) {
		/* and yet again the silly dance because of nul-terminators */
		char gnamebuf[sizeof(header->gname) + 1] = {0};
		memcpy(gnamebuf, header->gname, sizeof(header->gname));

		pw_getgid(gnamebuf, &out->gid);
	}

	/* If it is a symbolic/hard link, read the link target */
	if (out->type & (TCB_TYPE_SYMLINK|TCB_TYPE_HARDLINK))
		out->linkname = strdup(header->linkname);

	/* Extract device major and minor id */
	if (out->type & (TCB_TYPE_CDEV|TCB_TYPE_BLOCKDEV)) {
		out->dev_major = parse_octal(header->devmajor, sizeof(header->devmajor));
		out->dev_minor = parse_octal(header->devminor, sizeof(header->devminor));
	}

	return 0;
}

int
is_ustar(const struct header_posix_ustar *header)
{
	char ustar_magic[] = "ustar";
	if (strncmp(header->magic, ustar_magic, sizeof(ustar_magic)))
		return 0;

	char version[] = "00";
	if (strncmp(header->version, version, sizeof(header->version)))
		return 0;

	return 1;
}

/* Good luck with this. */
void
write_ustar_header(int outfd, const tcb *tcb)
{
	struct header_posix_ustar header = {0};

	strncpy(header.name, tcb->name, sizeof(header.name));

	/* !! Keep in sync with the header !!
	 *
	 * Unfortunately there is no simple and sane way to avoid having
	 * these constants in the printf format. */
	snprintf(header.mode,   sizeof(header.mode),  "%06.6o ",    tcb->mode & 0777);
	snprintf(header.uid,    sizeof(header.uid),   "%06.6o ",    tcb->uid);
	snprintf(header.gid,    sizeof(header.gid),   "%06.6o ",    tcb->gid);
	snprintf(header.size,   sizeof(header.size),  "%011.11zo ", tcb->size);
	snprintf(header.mtime,  sizeof(header.mtime), "%011.11zo ", tcb->mtime);
	memset(header.checksum, ' ', sizeof(header.checksum));

	switch (tcb->type) {
	case TCB_TYPE_REGULAR:  header.typeflag[0] = '0'; break;
	case TCB_TYPE_HARDLINK: header.typeflag[0] = '1'; break;
	case TCB_TYPE_SYMLINK:  header.typeflag[0] = '2'; break;
	case TCB_TYPE_CDEV:     header.typeflag[0] = '3'; break;
	case TCB_TYPE_BLOCKDEV: header.typeflag[0] = '4'; break;
	case TCB_TYPE_DIR:      header.typeflag[0] = '5'; break;
	case TCB_TYPE_NPIPE:    header.typeflag[0] = '6'; break;
	case TCB_TYPE_RESERVED: header.typeflag[0] = '7'; break;
	default:                header.typeflag[0] = 'q'; break;	/* WAT ? */
	}

	if (tcb->type & (TCB_TYPE_HARDLINK|TCB_TYPE_SYMLINK))
		strncpy(header.linkname, tcb->linkname, sizeof(header.linkname));

	strncpy(header.magic, "ustar", sizeof(header.magic));
	strncpy(header.version, "00", sizeof(header.version));

	pw_getuname(header.uname, sizeof(header.uname), tcb->uid);
	pw_getgname(header.gname, sizeof(header.gname), tcb->gid);


	snprintf(header.devmajor, sizeof(header.devmajor), "%06.6o", tcb->dev_major);
	snprintf(header.devminor, sizeof(header.devminor), "%06.6o", tcb->dev_minor);

	unsigned checksum = 0;
	for (size_t i = 0; i < sizeof(header); ++i) {
		checksum += (unsigned)((unsigned char *)(&header))[i];
	}
	snprintf(header.checksum, sizeof(header.checksum), "%06.6o ", checksum);
	header.checksum[6] = '\0';
	header.checksum[7] = ' ';

	xwrite(outfd, &header, sizeof(header));
}

void
ustar_fini(int outfd)
{
	struct header_posix_ustar empty = {0};
	xwrite(outfd, &empty, sizeof(empty));
	xwrite(outfd, &empty, sizeof(empty));
}
