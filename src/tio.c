/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "align.h"
#include "tio.h"

#include <assert.h>
#include <err.h>
#include <unistd.h>

void
xwrite(int fd, void *_buffer, size_t buffer_size)
{
	char   *buffer = _buffer;
	size_t  bytes  = 0;

	do {
		ssize_t rc = write(fd, buffer + bytes, buffer_size - bytes);

		if (rc <= 0)
			err(1, "write");

		bytes += rc;
	} while (bytes < buffer_size);
}

static ssize_t
_xread_record(int fd, void *_buffer)
{
	ssize_t  bytes  = 0;
	char    *buffer = _buffer;

	do {
		ssize_t r = read(fd, buffer + bytes, 512 - bytes);
		if (r <= 0)
			return r;

		bytes += r;
	} while (bytes < 512);

	return bytes;
}


ssize_t
xread(int fd, void *__buffer, size_t bytes)
{
	char     buffer[512];
	ssize_t  outbytes = 0;
	char    *_buffer  = __buffer;

	assert(align(bytes, 512) == bytes);

	while ((size_t)(outbytes) < bytes) {
		ssize_t rc;

		if (_buffer)
			rc = _xread_record(fd, _buffer + outbytes);
		else
			rc = _xread_record(fd, buffer);

		if (rc <= 0)
			return rc;

		outbytes += rc;
	}

	return outbytes;
}
