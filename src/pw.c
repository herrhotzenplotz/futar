/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "pw.h"

#include <assert.h>
#include <grp.h>
#include <string.h>

int
pw_getuid(const char *login, uid_t *out)
{
	assert(out);

	struct passwd *entry = getpwnam(login);
	if (!entry)
		return -1;

	*out = entry->pw_uid;

	return 0;
}

int
pw_getgid(const char *group, gid_t *out)
{
	assert(out);

	struct group *entry = getgrnam(group);
	if (!entry)
		return -1;

	*out = entry->gr_gid;

	return 0;
}

int
pw_getuname(char *buffer, size_t buffer_size, uid_t uid)
{
	assert(buffer);

	struct passwd *entry = getpwuid(uid);
	if (!entry)
		return -1;

	size_t namelen = strlen(entry->pw_name);
	assert(namelen < buffer_size);

	memcpy(buffer, entry->pw_name, namelen);
	buffer[namelen] = '\0';

	return 0;
}

int
pw_getgname(char *buffer, size_t buffer_size, gid_t gid)
{
	assert(buffer);

	struct group *entry = getgrgid(gid);
	if (!entry)
		return -1;

	size_t namelen = strlen(entry->gr_name);
	assert(namelen < buffer_size);

	memcpy(buffer, entry->gr_name, namelen);
	buffer[namelen] = '\0';

	return 0;
}
