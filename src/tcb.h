/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TCB_H
#define TCB_H

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

typedef enum {
	TCB_TYPE_REGULAR  = 0x01,
	TCB_TYPE_HARDLINK = 0x02,
	TCB_TYPE_SYMLINK  = 0x04,
	TCB_TYPE_CDEV     = 0x08,
	TCB_TYPE_BLOCKDEV = 0x10,
	TCB_TYPE_DIR      = 0x20,
	TCB_TYPE_NPIPE    = 0x40,
	TCB_TYPE_RESERVED = 0x80,
	TCB_TYPE_OTHER    = 0x00, /* Should be treated as a regular file. */
} tcb_type;

/* Tape control block */
typedef struct tcb {
	char     *name;         /* file name */
	char     *linkname;     /* symlink/hardlink target */
	mode_t    mode;         /* file mode */
	uid_t     uid;          /* User id */
	gid_t     gid;          /* Group id */
	size_t    size;         /* file size */
	time_t    mtime;        /* modification time */
	tcb_type  type;         /* type of this tcb */
	int       dev_major;    /* major device id */
	int       dev_minor;    /* minor device id */
} tcb;

void free_tcb(tcb *);
void print_tcb(const tcb *const);
void stat_to_tcb(const char *path, const struct stat *const buf, tcb *out);

#endif /* TCB_H */
