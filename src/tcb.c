/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "options.h"
#include "pw.h"
#include "tcb.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

void
free_tcb(tcb *it)
{
	if (!it)
		return;

	if (it->type & (TCB_TYPE_SYMLINK|TCB_TYPE_HARDLINK))
		free(it->linkname);

	free(it->name);
}

/* Translate a file mode (mode_t) into the human-readable rwx
 * format. */
static int
snprintmode(char *buffer, const size_t buffer_size, const mode_t mode)
{
	if (!buffer)
		return -1;
	if (buffer_size < (1 + 3 + 3 + 3))
		return -1;

	char suid = mode & S_ISUID ? 's' : '-';
	char ur   = mode & S_IRUSR ? 'r' : '-';
	char uw   = mode & S_IWUSR ? 'w' : '-';
	char ux   = mode & S_IXUSR ? 'x' : '-';

	char gr = mode & S_IRGRP ? 'r' : '-';
	char gw = mode & S_IWGRP ? 'w' : '-';
	char gx = mode & S_IXGRP ? 'x' : '-';

	char or = mode & S_IROTH ? 'r' : '-';
	char ow = mode & S_IWOTH ? 'w' : '-';
	char ox = mode & S_IXOTH ? 'x' : '-';

	snprintf(buffer, buffer_size, "%c%c%c%c%c%c%c%c%c%c",
	         suid,
	         ur, uw, ux,
	         gr, gw, gx,
	         or, ow, ox);

	return 0;
}

void
print_tcb(const tcb *const it)
{
	char modebuf[11] = {0};
	char timebuf[32] = {0};
	char tmpbuf[32] = {0};

	/* Verbose */
	if (vflag) {
		/* We want to print in ls(1) style :
		 *
		 *     drwxrwxrwt  11 root  wheel  uarch   12B Jun 21 12:04 ./
		 *
		 * First the mode, the next we don't have, then the owner and
		 * group, size, mtime and name last.
		 */

		if (snprintmode(modebuf, sizeof(modebuf), it->mode) < 0)
			errx(1, "error: cannot convert file mode");

		/* Special treatment of non-regular files */
		if (!(it->type & TCB_TYPE_REGULAR)) {
			if (it->type & TCB_TYPE_DIR)
				modebuf[0] = 'd';
			else if (it->type & TCB_TYPE_CDEV)
				modebuf[0] = 'c';
			else if (it->type & TCB_TYPE_BLOCKDEV)
				modebuf[0] = 'b'; /* Is this right ? */
			else if (it->type & TCB_TYPE_NPIPE)
				modebuf[0] = 'p';
			else if (it->type & TCB_TYPE_HARDLINK)
				modebuf[0] = 'H'; /* following star */
			else if (it->type & TCB_TYPE_SYMLINK)
				modebuf[0] = 'l'; /* following star */
		}

		/* TODO: How about some error checking? This is too much UNIX style. */
		strftime(timebuf, sizeof(timebuf), "%a %d %b %Y %R", gmtime(&it->mtime));

		/* mode and size */
		fprintf(stderr, "%s  %8zu", modebuf, it->size);

		/* Owner */
		if (pw_getuname(tmpbuf, sizeof(tmpbuf), it->uid) == 0)
			fprintf(stderr, "  %s", tmpbuf);
		else
			fprintf(stderr, "  %d", it->uid);

		/* Group */
		if (pw_getgname(tmpbuf, sizeof(tmpbuf), it->gid) == 0)
			fprintf(stderr, "  %s", tmpbuf);
		else
			fprintf(stderr, "  %d", it->gid);

		/* mtime and name */
		fprintf(stderr, "  %s  %s", timebuf, it->name);

		/* Symlinks */
		if (it->type & TCB_TYPE_SYMLINK)
			fprintf(stderr, " -> %s", it->linkname);

		/* Hardlink */
		if (it->type & TCB_TYPE_HARDLINK)
			fprintf(stderr, " link to %s", it->linkname);

		fprintf(stderr, "\n");
	} else {
		/* if not verbose just print the name */
		fprintf(stderr, "%s\n", it->name);
	}
}

void
stat_to_tcb(const char *path, const struct stat *const buf, tcb *out)
{
	out->name  = strdup(path);
	out->mode  = buf->st_mode;
	out->uid   = buf->st_uid;
	out->gid   = buf->st_gid;
	out->size  = buf->st_size;
	out->mtime = buf->st_mtim.tv_sec;

	if (S_ISREG(buf->st_mode))
		out->type = TCB_TYPE_REGULAR;
	else if (S_ISLNK(buf->st_mode))
		out->type = TCB_TYPE_SYMLINK;
	else if (S_ISCHR(buf->st_mode))
		out->type = TCB_TYPE_CDEV;
	else if (S_ISBLK(buf->st_mode))
		out->type = TCB_TYPE_BLOCKDEV;
	else if (S_ISDIR(buf->st_mode))
		out->type = TCB_TYPE_DIR;
	else if (S_ISFIFO(buf->st_mode))
		out->type = TCB_TYPE_NPIPE;
	else
		errx(1, "unsupported file type 0x%X", buf->st_mode & S_IFMT);

	if (out->type != TCB_TYPE_REGULAR)
		out->size = 0;

	if (out->type & (TCB_TYPE_CDEV|TCB_TYPE_BLOCKDEV)) {
		out->dev_major = major(buf->st_dev);
		out->dev_minor = minor(buf->st_dev);
	}
}
