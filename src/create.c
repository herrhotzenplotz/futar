/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "create.h"
#include "ustar.h"
#include "options.h"
#include "tio.h"

#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

static struct creatstat {
	char   **dirs;
	size_t	 dirs_size;
} cstat;

static void
cstat_cleanup(void)
{
	for (size_t i = 0; i < cstat.dirs_size; ++i) {
		free(cstat.dirs[i]);
	}
	free(cstat.dirs);
	cstat.dirs = NULL;
	cstat.dirs_size = 0;
}

static void
cpushdir(const char *dir)
{
	cstat.dirs = realloc(
		cstat.dirs, sizeof(*cstat.dirs) * (cstat.dirs_size + 1));

	if (!cstat.dirs)
		err(1, "cannot allocate memory");

	cstat.dirs[cstat.dirs_size++] = strdup(dir);
}

static int
ccheckdir(const char *dir)
{
	for (size_t i = 0; i < cstat.dirs_size; ++i)
		if (strcmp(cstat.dirs[i], dir) == 0)
			return 1;
	return 0;
}

static void
makedir(int tape, const char *path, const struct stat *buf)
{
	tcb    tcb = {0};
	size_t pathlen;

	pathlen = strlen(path);

	if (ccheckdir(path))
		return; /* Already created */

	cpushdir(path);

	stat_to_tcb(path, buf, &tcb);

	if (vflag) {
		fprintf(stderr, "a ");
		print_tcb(&tcb);
	}

	write_ustar_header(tape, &tcb);

	/* Do recurse */
	{
		DIR				*dirp;
		struct dirent	*entry;

		dirp = opendir(path);
		if (!dirp)
			errx(1, "cannot allocate memory");

		errno = 0;

		while ((entry = readdir(dirp))) {
			char *other_path = NULL;
			if (strncmp(entry->d_name, ".", entry->d_namlen) == 0
			    || strncmp(entry->d_name, "..", entry->d_namlen) == 0)
				continue; /* skip . and .. */

			other_path = calloc(entry->d_namlen + pathlen + 1 + 1, 1);

			memcpy(other_path, path, pathlen);
			other_path[pathlen] = '/';
			memcpy(other_path + pathlen + 1, entry->d_name, entry->d_namlen);
			other_path[entry->d_namlen + pathlen + 1] = '\0';

			addfile(tape, other_path);
			free(other_path);
			other_path = NULL;
		}

		if (errno)
			err(1, "cannot read directory");

		closedir(dirp);
	}
}

static struct inotab {
	ino_t  inode;
	char  *name;
} *inotab = NULL;
static size_t inotab_size = 0;

static void
inotab_push(const char *path, ino_t inode)
{
	inotab = realloc(inotab, sizeof(*inotab) * (inotab_size + 1));
	inotab[inotab_size].name = strdup(path);
	inotab[inotab_size].inode = inode;

	inotab_size += 1;
}

static const char *
inotab_find(ino_t inode)
{
	for (size_t i = 0; i < inotab_size; ++i) {
		if (inotab[i].inode == inode)
			return inotab[i].name;
	}
	return NULL;
}

static void
inotab_cleanup(void)
{
	for (size_t i = 0; i < inotab_size; ++i) {
		free(inotab[i].name);
	}
	free(inotab);
	inotab = NULL;
	inotab_size = 0;
}

static void
makereg(int tape, const char *path, const struct stat *buf)
{
	tcb	    tcb      = {0};
	int	    fd	     = 0;
	const char *linkname = NULL;

	stat_to_tcb(path, buf, &tcb);

	/* check if we want to insert a hardlink */
	if (buf->st_nlink > 1) {
		if ((linkname = inotab_find(buf->st_ino))) {
			/* Make it a hardlink */
			tcb.type = TCB_TYPE_HARDLINK;
			tcb.size = 0;
			tcb.linkname = strdup(linkname);
		} else {
			/* Make an entry in the inotab and write the file to disk,
			 * we are yet to encounter a link to this file. */
			inotab_push(path, buf->st_ino);
		}
	}

	if (vflag) {
		fprintf(stderr, "a ");
		print_tcb(&tcb);
	}

	write_ustar_header(tape, &tcb);

	/* Only open the file and dump it to the tape if we don't write a
	 * link. */
	if (!linkname) {
		if ((fd = open(path, O_RDONLY)) < 0)
			err(1, "cannot open '%s'", path);

		for (;;) {
			char buffer[512] = {0};
			ssize_t rc;

			rc = read(fd, buffer, sizeof(buffer));
			if (rc < 0)
				err(1, "read from '%s'", path);

			if (rc == 0) /* EOF */
				break;

			xwrite(tape, buffer, sizeof(buffer));
		}
		close(fd);
	}

	free_tcb(&tcb);
}

static void
makesym(int tape, const char *path, const struct stat *buf)
{
	char    tmp[100] = {0};	/* this is the ustar max length of the link */
	ssize_t	len      = 0;
	tcb	tcb      = {0};

	if ((len = readlink(path, tmp, sizeof(tmp))) < 0)
		err(1, "readlink of '%s'", path);

	stat_to_tcb(path, buf, &tcb);
	tcb.linkname = calloc(len + 1, 1);
	memcpy(tcb.linkname, tmp, len);

	if (vflag) {
		fprintf(stderr, "a ");
		print_tcb(&tcb);
	}

	write_ustar_header(tape, &tcb);
	free_tcb(&tcb);
}

static void
makespecial(int tape, const char *path, const struct stat *buf)
{
	tcb tcb = {0};

	stat_to_tcb(path, buf, &tcb);
	if (vflag) {
		fprintf(stderr, "a ");
		print_tcb(&tcb);
	}

	write_ustar_header(tape, &tcb);
	free_tcb(&tcb);
}

void
addfile(int tape, const char *path)
{
	struct stat buf = {0};

	if (lstat(path, &buf) < 0)
		err(1, "cannot stat '%s'", path);

	if (S_ISDIR(buf.st_mode)) {
		makedir(tape, path, &buf);
	} else if (S_ISREG(buf.st_mode)) {
		makereg(tape, path, &buf);
	} else if (S_ISLNK(buf.st_mode)) {
		makesym(tape, path, &buf);
	} else if (S_ISFIFO(buf.st_mode) || S_ISBLK(buf.st_mode) || S_ISCHR(buf.st_mode)) {
		makespecial(tape, path, &buf);
	} else {
		warnx("warning: skipping '%s': unsupported file type", path);
	}
}

void
create_cleanup(void)
{
	cstat_cleanup();
	inotab_cleanup();
}
