/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "compress.h"
#include "options.h"

#include <err.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int xflag        = 0;
int tflag        = 0;
int vflag        = 0;
int hflag        = 0;
int cflag        = 0;
int compressflag = 0;

/* The options are futar are not POSIX compliant as the POSIX tar is a
 * mess. Simplify and make it sane. */
static void
usage(void)
{
	const char *progname = getprogname();

	fprintf(stderr, "usage: %s -t [options] tarfiles...\n",      progname);
	fprintf(stderr, "       %s -x [options] tarfiles...\n",      progname);
	fprintf(stderr, "       %s -c [options] tarfile files...\n", progname);
	fprintf(stderr, "       %s -h\n",                            progname);
	fprintf(stderr, "\n");
	fprintf(stderr, "  The options may be one or more of the following:\n");
	fprintf(stderr, "    --xz               handle xz compression\n");
	fprintf(stderr, "    --bz               handle bzip2 compression\n");
	fprintf(stderr, "    --gz               handle gzip compression\n");
	fprintf(stderr, "    --zstd             handle zstd compression\n");
	fprintf(stderr, "    -v / --verbose     be verbose\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "   The modes may also be given as a long option:\n");
	fprintf(stderr, "     -t / --list       list contents of the archive\n");
	fprintf(stderr, "     -c / --create     create an archive\n");
	fprintf(stderr, "     -x / --extract    extract contents of an archive\n");
	fprintf(stderr, "     -h / --help       print this help\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>\n");
	fprintf(stderr, "Report bugs via E-Mail please (German/Dutch/English)\n");
}

static void
eusage(const char *fmt, ...)
{
	va_list vp;

	usage();
	va_start(vp, fmt);
	verrx(1, fmt, vp);
	va_end(vp);

	exit(EXIT_FAILURE);
}

void
parseflags(int *argc, char ***argv)
{
	int ch, li;

	/* options descriptor */
	static struct option longopts[] = {
		{ "create",  no_argument, NULL, 'c' },
		{ "extract", no_argument, NULL, 'x' },
		{ "verbose", no_argument, NULL, 'v' },
		{ "list",    no_argument, NULL, 't' },
		{ "xz",      no_argument, NULL, 1   },
		{ "gz",      no_argument, NULL, 1   },
		{ "bz",      no_argument, NULL, 1   },
		{ "zstd",    no_argument, NULL, 1   },
		{ "help",    no_argument, NULL, 'h' },
		{ NULL,      0,           NULL, 0   }
	};

	while ((ch = getopt_long(*argc, *argv, "+xvcth", longopts, &li)) != -1) {
		switch (ch) {
		case 't':
			tflag = 1;
			break;
		case 'v':
			vflag = 1;
			break;
		case 'x':
			xflag = 1;
			break;
		case 'h':
			hflag = 1;
			break;
		case 'c':
			cflag = 1;
			break;
		case 1:
			if (strcmp(longopts[li].name, "xz") == 0)
				compressflag |= COMPRESS_XZ;
			else if (strcmp(longopts[li].name, "gz") == 0)
				compressflag |= COMPRESS_GZIP;
			else if (strcmp(longopts[li].name, "bz") == 0)
				compressflag |= COMPRESS_BZIP2;
			else if (strcmp(longopts[li].name, "zstd") == 0)
				compressflag |= COMPRESS_ZSTD;
			break;
		case 0:	/* long-only opt */
			break;
		default:
			eusage("error on command line");
		}
	}
	*argc -= optind;
	*argv += optind;

	checkflags();
}

void
checkflags(void)
{
	/* process help request first */
	if (hflag) {
		usage();
		exit(EXIT_SUCCESS);
	}

	if (tflag) { /* List mode */
		if (xflag)
			eusage("error: cannot use -t and -x together");
		if (cflag)
			eusage("error: cannot use -t and -c together");

		if (compressflag)
			eusage("error: compression is not yet supported in list mode");
	}

	if (xflag) {/* Extract mode */
		if (tflag)
			eusage("error: cannot use -x and -t together");
		if (cflag)
			eusage("error: cannot use -x and -c together");

		if (compressflag)
			eusage("error: compression is not yet supported in extract mode");
	}

	if (cflag) { /* Create mode */
		if (tflag)
			eusage("error: cannot use -c and -t together");
		if (xflag)
			eusage("error: cannot use -c and -x together");
	}

	if (compressflag) {
		int c = compressflag;
		do {
			/* if the first bit is set and also any of the others */
			if ((c & 1) && (c & ~(0x1)))
				errx(1, "too many compression schemes. please choose one.");
		} while (c >>= 1);
	}

	/* Check that we have at least one mode */
	if (!(xflag || tflag || cflag))
		eusage("error: no operational mode given. (-c, -x or -t)");
}
