/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nsonack@herrhotzenplotz.de> wrote this file.  As long as you retain this
 * notice you can do whatever you want with this stuff. If we meet some day, and
 * you think this stuff is worth it, you can buy me a beer in return.Nico Sonack
 * ----------------------------------------------------------------------------
 */

#ifndef PW_H
#define PW_H

#include <sys/types.h>
#include <pwd.h>

/* User and group database handling */

int pw_getuid(const char *login, uid_t *);
int pw_getgid(const char *group, gid_t *);

int pw_getuname(char *buffer, size_t buffer_size, uid_t);
int pw_getgname(char *buffer, size_t buffer_size, gid_t);

#endif /* PW_H */
