/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "compress.h"
#include "options.h"
#include "tio.h"

#include <err.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static pid_t compresschild = 0;

static const char *
getcompressprog(void)
{
	if (compressflag & COMPRESS_XZ)
		return "xz";
	else if (compressflag & COMPRESS_GZIP)
		return "gzip";
	else if (compressflag & COMPRESS_BZIP2)
		return "bzip2";
	else if (compressflag & COMPRESS_ZSTD)
		return "zstd";

	errx(1, "unknown compression scheme");
	return NULL;
}

static void
childhdl(int sig, siginfo_t *info, void *uctx)
{
	(void) uctx;

	if (info->si_pid == compresschild) {
		if (info->si_status)
			errx(info->si_status,
			     "compression child exited with error %d on signal %d",
			     info->si_status, sig);
	}
}

static void
detectcompress(int tapefd)
{
	/* Here are a couple of file magics */
	static const char xz_magic[]   = { 0xFD, 0x37, 0x7A, 0x58, 0x5A, 0x00 };
	static const char gz_magic[]   = { 0x1F, 0x8B };
	static const char bz_magic[]   = { 0x42, 0x5A, 0x68 };
	static const char zstd_magic[] = { 0xFD, 0x2F, 0xB5, 0x28 };


	/* if the user gave us a compression type, we shall use it, if we
	 * are in create mode, this is nonesense anyways. */
	if (compressflag || cflag)
		return;

	/* otherwise, assume we can seek to read a couple of bytes and
	 * look for a magic. if we find a magic corresponding with a
	 * compression algo, set the flag, emit a warning and seek back to
	 * the beginning. if it fails, we are likely looking at a pipe or
	 * a tape that is not seekable. error out in that case for now. in
	 * the future we might insert a buffering media-changer process
	 * here. */
	char record[512];
	if (xread(tapefd, record, sizeof(record)) < 0)
		err(1, "cannot read tape");

	errno = 0;
	/* do detection */
	if (memcmp(record, xz_magic, sizeof(xz_magic)) == 0)
		compressflag |= COMPRESS_XZ;
	else if (memcmp(record, bz_magic, sizeof(bz_magic)) == 0)
		compressflag |= COMPRESS_BZIP2;
	else if (memcmp(record, gz_magic, sizeof(gz_magic)) == 0)
		compressflag |= COMPRESS_GZIP;
	else if (memcmp(record, zstd_magic, sizeof(zstd_magic)) == 0)
		compressflag |= COMPRESS_ZSTD;

	if (compressflag) {
		warnx("warning: archive is %s-compressed. "
		      "better specify the corresponding flag.",
		      getcompressprog());
	}

	/* seek back to the beginning */
	if (lseek(tapefd, 0, SEEK_SET) < 0)
		err(1, "cannot seek (this might not work with pipes)"); /* FIXME : handle tapes */
}

int
compressopen(int tapefd)
{
	int		 pair[2];
	struct sigaction sa;

	detectcompress(tapefd);

	if (!compressflag)
		return tapefd;

	sa.sa_sigaction = childhdl;
	sa.sa_flags = SA_SIGINFO|SA_RESTART;

	if (sigaction(SIGCHLD, &sa, NULL) < 0)
		err(1, "sigaction");

	if (pipe(pair) < 0)
		err(1, "pipe");

	if ((compresschild = fork()) < 0)
		err(1, "fork");

	if (compresschild == 0) { /* child */
		const char *const prg = getcompressprog();

		close(pair[0]);

		if (cflag) {			/* we are writing to tape */
			if (dup2(pair[1], STDIN_FILENO) < 0) /* stdin = pipe end */
				err(1, "dup2");

			if (dup2(tapefd, STDOUT_FILENO) < 0) /* stdout = tape */
				err(1, "dup2");

			if (execlp(prg, prg, NULL) < 0) /* hand over to compress program */
				err(1, "execlp");
		} else {				/* we are reading from tape */
			if (dup2(pair[1], STDOUT_FILENO) < 0) /* stdout = pipe in */
				err(1, "dup2");

			if (dup2(tapefd, STDIN_FILENO) < 0) /* stdin = tape */
				err(1, "dup2");

			if (execlp(prg, prg, "-dc", NULL) < 0) /* hand over to decompressor */
				err(1, "execlp");
		}

		/* Not reached */
		exit(EXIT_FAILURE);
	} else { /* parent */
		close(tapefd);
		close(pair[1]);

		return pair[0];
	}
}

void
compressclose(void)
{
	if (compresschild) {
		int result;
		if (waitpid(compresschild, &result, WEXITED) < 0)
			err(1, "waitpid");

		if (WIFSIGNALED(result)) {
			int sig	 = WTERMSIG(result);
			errx(1, "compression child terminated due to signal %d", sig);
		} else if (WIFEXITED(result)) {
			int code = WEXITSTATUS(result);
			if (code)			/* forward the exit code */
				errx(code, "compression child exited with code '%d'", code);
		}
	}
}
