/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "align.h"
#include "extract.h"
#include "options.h"

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static int
fset_mtime(int fd, time_t mtime)
{
	struct timespec times[2] = {0};

	times[0].tv_sec	 = UTIME_OMIT;
	times[0].tv_nsec = UTIME_OMIT;
	times[1].tv_sec	 = mtime;
	times[1].tv_nsec = 0;

	if (futimens(fd, times) < 0)
		return -1;

	return 0;
}

static int
set_mtime(const char *path, time_t mtime)
{
	int fd = 0;

	/* We don't wanna follow symlinks as we want to set mtime on
	 * themselves. */
	fd = open(path, O_RDWR|O_NOFOLLOW);
	if (fd < 0)
		return -1;

	if (fset_mtime(fd, mtime) < 0)
		warn("warning: could not set mtime of '%s'", path);

	close(fd);
	return 0;
}

static void
xchown(const char *const path, uid_t uid, gid_t gid)
{
	static uid_t euid         = 0;
	static int   euid_checked = 0;

	if (!euid_checked) {
		euid = geteuid();
		euid_checked = 1;
	}

	/* We can only chown to other user if we are effective root. */
	if (euid)
		uid = euid;

	if (lchown(path, uid, gid) < 0)
		warn("warning: unable to chown '%s'", path);
}

static int
makedir_rec(tcb *tcb)
{
	struct stat  buf = {0};
	char        *path, *p;
	int          end = 0;

	path = strdup(tcb->name);
	p = path;

	for (;;) {
		if (p[0] == '\0')
			end = 1;
		else if (p[0] != '/') {
			p++;
			continue;
		}


		p[0] = '\0';

		if (stat(path, &buf) < 0) {
			/* Seems to not exist ? */
			if (mkdir(path, tcb->mode) < 0) {
				warn("cannot create directory '%s'", path);
				free(path);
				return -1;
			}

			xchown(path, tcb->uid, tcb->gid);
			set_mtime(path, tcb->mtime);
		} else {
			if (!S_ISDIR(buf.st_mode)) {
				warn("existing file '%s' is not a directory", path);
				free(path);
				return -1;
			}
		}

		if (end)
			break;

		p[0] = '/';
		++p;
	}

	free(path);
	return 0;
}

int
makedir(tcb *tcb)
{
	assert(tcb->type & TCB_TYPE_DIR);

	/* Check if it already exists */
	if (access(tcb->name, F_OK) == 0) {
		warnx("warning: directory '%s' already exists.", tcb->name);
		return 0;
	}

	if (errno != ENOENT) {
		warn("warning: failed to access directory '%s'", tcb->name);
		return -1;
	}

	/* Reset errno */
	errno = 0;

	if (makedir_rec(tcb) < 0)
		err(1, "error: could not make directory");

	return 0;
}

static void
setmeta(tcb *tcb)
{
	xchown(tcb->name, tcb->uid, tcb->gid);

	if (lchmod(tcb->name, tcb->mode) < 0)
		warn("warning: unable to chmod '%s'", tcb->name);

	set_mtime(tcb->name, tcb->mtime);
}

static void
makeregular(int fd, tcb *tcb)
{
	size_t bytes = 0;
	char   record[512];

	/* We open the file in a mode such that we have access to it */
	int outfd = open(tcb->name, O_WRONLY|O_CREAT, 0664);
	if (outfd < 0)
		err(1, "open");

	while (bytes < tcb->size) {
		ssize_t r = read(fd, record, sizeof(record));
		if (r == 0)
			errx(1, "error: unexpected eof while extracting file '%s'", tcb->name);
		if (r < 0)
			err(1, "read");

		bytes += r;

		if (write(outfd, record, r) != r)
			err(1, "write");
	}

	ftruncate(outfd, tcb->size);
	close(outfd);

	setmeta(tcb);
}

static void
makesym(tcb *tcb)
{
	if (symlink(tcb->linkname, tcb->name) < 0)
		warn("warning: could not create symlink '%s' -> '%s'",
		     tcb->name, tcb->linkname);

	setmeta(tcb);
}

static void
makelink(tcb *tcb)
{
	if (link(tcb->linkname, tcb->name) < 0)
		warn("warning: could not create link '%s' to '%s'",
		     tcb->name, tcb->linkname);

	setmeta(tcb);
}

static void
makefifo(tcb *tcb)
{
	if (mkfifo(tcb->name, tcb->mode) < 0)
		warn("warning: could not create fifo '%s'", tcb->name);

	setmeta(tcb);
}

/* The two following routines are there for completeness. I did not
 * test them as the entire idea of having character and block devices
 * in a tarfile seems obsolete. This is handled by the devfs
 * driver. If it doesn't work, too bad. */
static void
makecdev(tcb *tcb)
{
	dev_t  dev  = makedev(tcb->dev_major, tcb->dev_minor);
	mode_t mode = tcb->mode | S_IFCHR;

	if (mknod(tcb->name, mode, dev) < 0)
		warn("warning: could not create character device '%s'", tcb->name);
}

static void
makebdev(tcb *tcb)
{
	dev_t  dev  = makedev(tcb->dev_major, tcb->dev_minor);
	mode_t mode = tcb->mode | S_IFBLK;

	if (mknod(tcb->name, mode, dev) < 0)
		warn("warning: could not create block device '%s'", tcb->name);
}

int
extract(int fd, tcb *tcb)
{
	/* Only dump the file info if in verbose mode */
	if (vflag) {
		fprintf(stderr, "x ");
		print_tcb(tcb);
	}

	switch (tcb->type) {
	case TCB_TYPE_DIR:
		makedir(tcb);
		break;
	case TCB_TYPE_RESERVED:
		warnx("warning: '%s' is reserved. creating as regular file.", tcb->name);
	case TCB_TYPE_REGULAR:
		makeregular(fd, tcb);
		break;
	case TCB_TYPE_SYMLINK:
		makesym(tcb);
		break;
	case TCB_TYPE_HARDLINK:
		makelink(tcb);
		break;
	case TCB_TYPE_NPIPE:
		makefifo(tcb);
		break;
	case TCB_TYPE_CDEV:
		makecdev(tcb);
		break;
	case TCB_TYPE_BLOCKDEV:
		makebdev(tcb);
		break;
	default:
		warnx("warning: will not extract file '%s' (not implemented)",
		      tcb->name);
		if (lseek(fd, align(tcb->size, 512), SEEK_CUR) < 0)
			err(1, "error: cannot seek");
		break;
	}

	return 0;
}
