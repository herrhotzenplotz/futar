/*
 * Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <err.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "create.h"
#include "compress.h"
#include "extract.h"
#include "list.h"
#include "options.h"
#include "tio.h"
#include "tcb.h"
#include "ustar.h"

static int
is_empty_tar_header(struct header_posix_ustar *header)
{
	struct header_posix_ustar empty = {0};

	return !memcmp(header, &empty, sizeof(empty));
}

static int
xread_break(int fd, void *buffer, size_t nbytes)
{
	ssize_t rc = xread(fd, buffer, nbytes);

	if (rc == 0) {
		warnx("warning: unexpected eof seen in tape");
		return 1;
	}
	if (rc < 0)
		err(1, "read");

	return 0;
}

static int
doaction(int tape, int (*action)(int, tcb *))
{
	struct header_posix_ustar tar_header = {0};

	tape = compressopen(tape);

	for (;;) {
		/* Read the header record */
		if (xread_break(tape, &tar_header, sizeof(tar_header)))
			break;

		/* Try to find out whether we have reached the end of the tar
		 * archive. */
		if (is_empty_tar_header(&tar_header)) {
			if (xread_break(tape, &tar_header, sizeof tar_header))
				break;

			if (is_empty_tar_header(&tar_header)) {
				break;
			} else {
				warnx("warning: skipping empty tar block");
			}
		}

		{
			tcb controlblock = {0};

			if (ustar_read_header(&tar_header, &controlblock) < 0)
				errx(1, "error: cannot read ustar header");

			if (action(tape, &controlblock) < 0) {
				free_tcb(&controlblock);
				return -1;
			}

			free_tcb(&controlblock);
		}
	}

	compressclose();
	return 0;
}

static int
doextract(int tape)
{
	return doaction(tape, extract);
}

static int
dolist(int tape)
{
	return doaction(tape, list);
}

static pid_t compresschild = 0;

static void
docreate(int argc, char *argv[])
{
	int   outfd;
	char *tarfile;

	if (argc < 2)
		errx(1, "error: too few arguments for create mode");

	tarfile = *argv++;
	argc--;

	if (strcmp(tarfile, "-") == 0) {
		outfd = STDOUT_FILENO;
		if (isatty(outfd))
			errx(1, "error: refusing to create archive to stdout");
	} else {
		if ((outfd = open(tarfile, O_RDWR|O_CREAT), 0644) < 0)
			err(1, "error: cannot open tarfile");

		fchmod(outfd, 0644);
	}

	outfd = compressopen(outfd);

	for (int i = 0; i < argc; ++i)
		addfile(outfd, argv[i]);

	ustar_fini(outfd);
	close(outfd);
	compressclose();
	create_cleanup();

	if (compresschild) {
		int result;
		if (waitpid(compresschild, &result, WEXITED) < 0)
			err(1, "waitpid");

		if (WIFSIGNALED(result)) {
			int sig = WTERMSIG(result);
			errx(1, "compression child terminated due to signal %d", sig);
		} else if (WIFEXITED(result)) {
			int code = WEXITSTATUS(result);
			if (code) /* forward the exit code */
				errx(code, "compression child exited with code '%d'", code);
		}
	}

	exit(EXIT_SUCCESS);
}

int
main(int argc, char *argv[])
{
	int   fd      = 0;
	char *tarfile = NULL;

	parseflags(&argc, &argv);

	if (cflag)
		docreate(argc, argv);

	for (int i = 0; i < argc; ++i) {
		tarfile = *(argv++);

		if (strcmp("-", tarfile) == 0) {
			fd = STDIN_FILENO;
			if (isatty(fd))
				errx(1, "error: tape cannot be a tty");
		} else {
			if ((fd = open(tarfile, O_RDONLY)) < 0)
				err(1, "error: cannot open %s", tarfile);
		}

		if (xflag)
			doextract(fd);
		else if (tflag)
			dolist(fd);

		close(fd);
	}

	return 0;
}
