#
# Copyright 2022 Nico Sonack <nsonack@herrhotzenplotz.de>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

PROG	=	futar

SRCS	=	\
			src/compress.c	\
			src/create.c	\
			src/extract.c	\
			src/futar.c		\
			src/list.c		\
			src/options.c	\
			src/pw.c		\
			src/tio.c		\
			src/tcb.c		\
			src/ustar.c

OBJS	=	${SRCS:.c=.o}

# C11 because _Static_assert. Oh and yes, I intentionally override the
# environment. Thou shalt not change the C standard. You have been
# warned.
CFLAGS	=	-std=c11 -pedantic -Wall -Wextra -Werror -g -O0 -fPIC
LDFLAGS	+=	-pie

.PHONY: all clean

all: ${PROG}

${PROG}: ${OBJS}
	${CC} -o ${PROG} ${OBJS} ${LDFLAGS}

.SUFFIXS: .c .o
.c.o:
	${CC} -c ${CPPFLAGS} ${CFLAGS} -o $@ $<

clean:
	rm -f ${PROG} ${OBJS}
